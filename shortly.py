# _*_ coding: utf-8 _*_

def shortit(t):
    '''md5 short everything
    url='https://www.linode.com/?r=984e8c424bcadff4d5862c346688447ac625406e'
    print shortit(url)
    '''
    from hashlib import md5
    _salt = 'wert'
    _seed = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    _hex = md5(_salt + t).hexdigest()
    _hexLen = len(_hex)
    _subHexLen = _hexLen / 8
    _output = []
    for i in xrange(0, _subHexLen):
        _subHex = _hex[i*8:i*8+8]
        _subHex = 0x3FFFFFFF&int(1*('0x%s'%_subHex), 16)
        _o = []
        for n in xrange(0, 6):
            _index = 0x0000003D & _subHex
            _o.append(_seed[int(_index)])
            _subHex = _subHex >> 5
        _output.append(''.join(_o))
    return _output



