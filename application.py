# -*- coding: utf-8 -*-
__author__ = 'arkii'
__email__ = 'sqy6@163.com'
__create__ = '2015/01/06 15:09'

activate_this = '/home/service/pyenv/bin/activate_this.py'
execfile(activate_this, dict(__file__=activate_this))

import sqlite3
from flask import Flask, g, request, redirect

BASEURL = 'http://1.arkii.me/'
DATABASE = 'shortly.db'

app = Flask(__name__)


def connect_db():
    return sqlite3.connect(DATABASE)


def query_db(query, args=(), one=False):
    cur = g.db.execute(query, args)
    rv = [dict((cur.description[idx][0], value)
               for idx, value in enumerate(row)) for row in cur.fetchall()]
    return (rv[0] if rv else None) if one else rv


def insert_db(query, args=()):
    cur = g.db.execute(query, args)
    g.db.commit()


@app.before_request
def before_request():
    g.db = connect_db()


@app.teardown_request
def teardown_request(exception):
    if hasattr(g, 'db'):
        g.db.close()


@app.route('/shortly', methods=['GET', 'POST'])
def shortsvc(url=None):
    if request.method == 'POST':
        from shortly import shortit

        link = request.form['link']
        key = shortit(link)[0]
        insert_db("insert into shortly (key, link) values (?, ?)", (key, link))
        return BASEURL + key
    else:
        return '''
    <!doctype html>
    <html>
    <body>
    <form action='/shortly' method='post'>
         <input type='text' name='link' size='50'>
         <input type='submit' value='shortit'>
    </form>
    '''


@app.route('/<url>')
def urlsvc(url=None):
    record = query_db('select * from shortly where key = ?', [url], one=True)
    if record is None:
        return 'No such URL'
    else:
        return redirect(record['link'])



@app.route('/ip')
def echoip():
    return request.remote_addr



if __name__ == '__main__':
    app.run(host='0.0.0.0')
    app.run(debug=True)
