DROP TABLE IF EXISTS `shortly`;
CREATE TABLE `shortly` (
  `id` INTEGER PRIMARY KEY AUTOINCREMENT,
  `key` char(6) NOT NULL,
  `link` varchar(255) NOT NULL,
unique(key)
) 
